const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const routes = express.Router();
const User = require('../model/userschema');

routes.get('/', (req, res) => {
    res.send('hello world from the server');
});

routes.get('/about', (req, res) => {
    res.send('this is about page content');
});

routes.get('/contact', (req, res) => {
    res.send('this is our contact page');
});
// routes.post('/register', (req, res) => {
//     const { name, email, mobile, address, speciality, technical } = req.body;

//     if (!name || !email || !mobile || !address || !speciality || !technical) {
//         return res.status(422).json({ status: 'plz fill all the fields' });
//     }
//     console.log(name);
//     console.log(email);
//     User.findOne({ email: email }).then((userexist) => {
//             if (userexist) {
//                 return res.status(422).json({ status: 'Email already exists' });
//                 // res.send("user already exist with this email id");
//             }
//             const user = new User(req.body);
//             user.save().then(() => {
//                 return res.status(201).json({ successfully: 'User saved successfully' });
//                 // res.send("User saved successfully");
//             }).catch((err) => res.status(500).json({ error: 'Failed to register' }));


//         })
//         // res.send('my register page');
// });

routes.post('/register', async(req, res) => {
    const { name, email, password, mobile, address, speciality, technical } = req.body;

    if (!name || !email || !mobile || !address || !speciality || !technical) {
        return res.status(422).json({ status: 'plz fill all the fields' });
    }

    try {
        const getUser = await User.findOne({ email: email });
        if (getUser) {
            return res.status(422).json({ status: 'Email already exists' });
        }
        const user = new User(req.body);
        const registerUser = await user.save();

        if (registerUser) {
            return res.status(201).json({ successfully: 'User saved successfully' });
        } else {
            return res.status(500).json({ error: 'Failed to register' });
        }
    } catch (err) {
        console.log(err);
    }
});

routes.post('/signin', async(req, res) => {
    // console.log(req.body);

    try {
        let token;
        const { email, password } = req.body;
        const loginUser = await User.findOne({ email: email });
        // console.log(loginUser);

        if (loginUser) {
            const isLogin = await bcrypt.compare(password, loginUser.password);
            token = await loginUser.generateAuthToken();
            console.log(token);
            if (!isLogin) {
                res.status(400).json({ 'error': 'invalid credentials' });
            } else {
                res.status(400).json({ 'success': 'user found' });
            }

        } else {
            res.status(404).json({ 'error': 'invalid credentials' });
        }

    } catch (err) {
        console.log(err);
    };
});

module.exports = routes;
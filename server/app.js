const express = require('express');

const dotenv = require('dotenv');
dotenv.config({ path: './config.env' });
require('./db/conn.js');
const app = express();

const User = require('./model/userschema');

const createUser = async() => {
    try {
        const myUser = new User({
            name: "shubham1",
            email: "xyz2@gmail.com",
            address: "aligarh123",
            mobile: "8734756347",
            speciality: 'sw developer',
            technical: 'sw sw sw sw sw'
        });
        const result = await myUser.save();
        console.log(result);
    } catch (err) {
        console.log(err);
    }

}

// createUser();

app.use(express.json());

app.use(require('./routes/route'));

app.listen(3000, () => {
    console.log('server is running at port 3000');
});